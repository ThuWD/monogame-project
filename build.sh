#! /usr/bin/env bash

dotnet publish /p:PublishReadyToRun=true /p:PublishSingleFile=true -r linux-x64 -c Release --self-contained
dotnet publish /p:PublishReadyToRun=true /p:PublishSingleFile=true -r linux-arm -c Release --self-contained
dotnet publish /p:PublishReadyToRun=true /p:PublishSingleFile=true -r linux-arm64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r linux-musl-x64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r win-x64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r win-x86 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r win-arm -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r win-arm64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r osx-x64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r osx.10.11-x64 -c Release --self-contained
dotnet publish /p:PublishSingleFile=true -r osx.10.10-x64 -c Release --self-contained
