﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;


namespace monogame_project
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        Texture2D player_texture;
        Vector2 player_pos;
        float player_speed;
        private SpriteFont font;
        public String name = "";
        Texture2D bg_texture;

        public Game1()
        {
            
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }
        public void InitName() {
            Random rand = new Random();
            int num = rand.Next(2);
            Console.WriteLine(num);
            switch(num) {
                case 0: {
                    name = "Mr. Dickass";
                    break;
                }
                case 1: {
                    name = "Mr. Idk";
                    break;
                }
                default: {
                    name = "The developer has fucked up.";
                    break;
                }
            }
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            // _graphics.IsFullScreen = false;
            // _graphics.PreferredBackBufferWidth = 1280;
            // _graphics.PreferredBackBufferHeight = 720;
            // _graphics.Applychanges();
            
            _graphics.IsFullScreen = false;
            _graphics.PreferredBackBufferWidth = 1280;
            _graphics.PreferredBackBufferHeight = 720;

            _graphics.ApplyChanges();
            player_pos = new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2);
            player_speed = 200f;
            InitName();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            bg_texture = Content.Load<Texture2D>("bg");
            player_texture = Content.Load<Texture2D>("player");
            font = Content.Load<SpriteFont>("Arial");
            Console.WriteLine("Content Loaded");
        }

        protected override void Update(GameTime gameTime)
        {
            var kstate = Keyboard.GetState();
            Console.WriteLine(kstate);
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                Exit();
            }

            if (kstate.IsKeyDown(Keys.W))
                player_pos.Y -= player_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(kstate.IsKeyDown(Keys.S))
                player_pos.Y += player_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (kstate.IsKeyDown(Keys.A))
                player_pos.X -= player_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if(kstate.IsKeyDown(Keys.D))
                player_pos.X += player_speed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            base.Update(gameTime);


            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            // MouseState mouse = new MouseState();
            // Console.WriteLine(mouse);
            GraphicsDevice.Clear(Color.Aquamarine);
            // player_texture.Width = player_texture.Width * 2;
            // player_texture.Height = player_texture.Height * 2;
            // doesn't work

            _spriteBatch.Begin();
            _spriteBatch.Draw(
                bg_texture,
                new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2),
                new Rectangle(0,0,1280,720),
                Color.White,
                0f,
                new Vector2(bg_texture.Width / 2, bg_texture.Height / 2),
//                new Vector2(0,0),
                Vector2.One,
                SpriteEffects.None,
                0f
                );
            _spriteBatch.Draw(
                player_texture,
                player_pos,
                new Rectangle(0,0,32,64),
                Color.White,
                0f,
                new Vector2(player_texture.Width / 2, player_texture.Height / 2),
                Vector2.One,
                SpriteEffects.None,
                0f
            );
                
                _spriteBatch.DrawString(font, "Remember to wear condoms", new Vector2(0, 0), Color.DarkKhaki, 0, new Vector2(0,0), 1.0f, SpriteEffects.None, 0.5f);
                // Finds the center of the string in coordinates inside the text rectangle
                Vector2 textMiddlePoint = font.MeasureString(name) / 2;
                // Places text in center of the screen
                Vector2 textpos = player_pos;
                textpos.Y = textpos.Y - 40;
                Vector2 position = textpos;
                _spriteBatch.DrawString(font, name, position, Color.BlueViolet, 0, textMiddlePoint, 1.0f, SpriteEffects.None, 0.5f);

            _spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
